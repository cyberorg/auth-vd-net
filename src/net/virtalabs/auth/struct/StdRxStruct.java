package net.virtalabs.auth.struct;

public class StdRxStruct {
	protected String action;

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
}
