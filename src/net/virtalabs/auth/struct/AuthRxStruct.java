package net.virtalabs.auth.struct;

public class AuthRxStruct extends StdRxStruct {
	protected String token;

	public String getToken() {
		return token;
	}
}
