package net.virtalabs.auth;

import net.virtalabs.auth.exceptions.AppException;
import net.virtalabs.auth.helpers.DbHelper;
import net.virtalabs.auth.utils.C;

public class App {

	public static String run(String json) {
		try {
			return Router.route(json);
		} catch (AppException appEx) {
			// DB Close
			DbHelper.close();
			return appEx.handle();
		} catch (ClassCastException castEx) {
			return StdTx.reply(C.WRONG_JSON, "We got Malformed JSON");
		} catch (Exception e) {
			// DB Close
			DbHelper.close();
			e.printStackTrace(System.err);
			return StdTx.reply(C.GENERAL_ERROR, "General Exception occured.");
		}
	}

}
