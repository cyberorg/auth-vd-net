package net.virtalabs.auth.mod.registerApp.resetSecret;

import net.virtalabs.auth.struct.StdTxStruct;

@SuppressWarnings("unused")
public class ResetSecretTxStruct extends StdTxStruct {
	private String newSecret;

	public void setNewSecret(String newSecret) {
		this.newSecret = newSecret;
	}

}
