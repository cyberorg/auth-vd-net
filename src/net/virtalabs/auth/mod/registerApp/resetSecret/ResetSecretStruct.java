package net.virtalabs.auth.mod.registerApp.resetSecret;

import net.virtalabs.auth.struct.AuthRxStruct;

public class ResetSecretStruct extends AuthRxStruct {
	private String appId;

	public String getAppId() {
		return appId;
	}

}
