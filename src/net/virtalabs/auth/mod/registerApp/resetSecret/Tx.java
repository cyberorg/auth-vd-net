package net.virtalabs.auth.mod.registerApp.resetSecret;

import com.google.gson.Gson;

public class Tx {
	private static final ResetSecretTxStruct sTx = new ResetSecretTxStruct();
	private static final Gson gson = new Gson();

	static String talk(String newSecret) {

		sTx.setCode(0);
		sTx.setMessage("Sending data");

		sTx.setNewSecret(newSecret);

		// and return
		return gson.toJson(sTx);
	}
}
