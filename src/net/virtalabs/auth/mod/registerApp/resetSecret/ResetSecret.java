package net.virtalabs.auth.mod.registerApp.resetSecret;

import net.virtalabs.auth.exceptions.AppException;
import net.virtalabs.auth.helpers.DbHelper;
import net.virtalabs.auth.helpers.TokenHelper;
import net.virtalabs.auth.mod.registerApp.Generator;
import net.virtalabs.auth.models.Apps;
import net.virtalabs.auth.utils.C;

public class ResetSecret {

	public static String run(ResetSecretStruct data) throws AppException {
		// TODO check token
		DbHelper.open();
		// Check if appHasAccess
		Long appId = Long.parseLong(data.getAppId());
		Apps thisApp = TokenHelper.uid2App(data.getToken(), appId);

		// Re-generate secret
		String newSecret = Generator.generateAppSecret();
		thisApp.set("appSecret", newSecret);
		// save
		if (!thisApp.save()) {
			throw new AppException("Unable to update app record", C.DB_SAVE);
		}

		DbHelper.close();
		return Tx.talk(newSecret);
	}
}
