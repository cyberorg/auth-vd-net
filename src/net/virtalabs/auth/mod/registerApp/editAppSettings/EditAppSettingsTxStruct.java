package net.virtalabs.auth.mod.registerApp.editAppSettings;

import net.virtalabs.auth.struct.StdTxStruct;

@SuppressWarnings("unused")
public class EditAppSettingsTxStruct extends StdTxStruct {
	private String redirectUrls;

	public void setRedirectUrls(String redirectUrls) {
		this.redirectUrls = redirectUrls;
	}

}
