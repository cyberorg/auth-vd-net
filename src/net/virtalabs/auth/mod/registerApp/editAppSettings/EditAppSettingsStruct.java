package net.virtalabs.auth.mod.registerApp.editAppSettings;

import net.virtalabs.auth.struct.AuthRxStruct;

public class EditAppSettingsStruct extends AuthRxStruct {
	private String appId;
	private String redirectUrls;

	public String getRedirectUrls() {
		return redirectUrls;
	}

	public String getAppId() {
		return appId;
	}

}
