package net.virtalabs.auth.mod.registerApp.editAppSettings;

import com.google.gson.Gson;

public class Tx {
	private static final EditAppSettingsTxStruct sTx = new EditAppSettingsTxStruct();
	private static final Gson gson = new Gson();

	static String talk(String redirectUrls) {

		sTx.setCode(0);
		sTx.setMessage("Sending data");

		sTx.setRedirectUrls(redirectUrls);
		// and return
		return gson.toJson(sTx);
	}
}
