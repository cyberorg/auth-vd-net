package net.virtalabs.auth.mod.registerApp.editAppSettings;

import net.virtalabs.auth.exceptions.AppException;
import net.virtalabs.auth.helpers.DbHelper;
import net.virtalabs.auth.helpers.TokenHelper;
import net.virtalabs.auth.models.Apps;
import net.virtalabs.auth.utils.C;

public class EditAppSettings {

	public static String run(EditAppSettingsStruct data) throws AppException {
		DbHelper.open();
		// TODO check token

		// Check if appHasAccess
		Long appId = Long.parseLong(data.getAppId());
		Apps thisApp = TokenHelper.uid2App(data.getToken(), appId);

		thisApp.set("appRedirectUrls", data.getRedirectUrls());
		if (!thisApp.save()) {
			throw new AppException("Unable to save record", C.DB_SAVE);
		}
		DbHelper.close();
		return Tx.talk(data.getRedirectUrls());
	}
}
