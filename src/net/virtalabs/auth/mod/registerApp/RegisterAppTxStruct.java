package net.virtalabs.auth.mod.registerApp;

import net.virtalabs.auth.struct.StdTxStruct;

@SuppressWarnings("unused")
public class RegisterAppTxStruct extends StdTxStruct {
	private App data;

	static class App {
		private long appId;
		private String appSecret;
		private String redirectUrls;

		public void setAppId(long appId2) {
			this.appId = appId2;
		}

		public void setAppSecret(String appSecret) {
			this.appSecret = appSecret;
		}

		public void setRedirectUrls(String redirectUrls) {
			this.redirectUrls = redirectUrls;
		}
	}

	public void setData(App data) {
		this.data = data;
	}

}
