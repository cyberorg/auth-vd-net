package net.virtalabs.auth.mod.registerApp;

import com.google.gson.Gson;

public class Tx {
	private static final RegisterAppTxStruct sTx = new RegisterAppTxStruct();
	private static final Gson gson = new Gson();

	static String talk(long appId, String appSecret, String redirectUrls) {

		sTx.setCode(0);
		sTx.setMessage("Sending data");

		RegisterAppTxStruct.App data = new RegisterAppTxStruct.App();
		data.setAppId(appId);
		data.setAppSecret(appSecret);
		data.setRedirectUrls(redirectUrls);
		sTx.setData(data);
		// and return
		return gson.toJson(sTx);
	}
}
