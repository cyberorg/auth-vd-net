package net.virtalabs.auth.mod.registerApp;

import net.virtalabs.auth.struct.AuthRxStruct;

public class RegisterAppStruct extends AuthRxStruct {
	private App app;

	static class App {
		private String type;
		private String name;
		private String logo;
		private String url;
		private String redirects;

		public String getType() {
			return type;
		}

		public String getName() {
			return name;
		}

		public String getLogo() {
			return logo;
		}

		public String getUrl() {
			return url;
		}

		public String getRedirects() {
			return redirects;
		}

	}

	public App getApp() {
		return app;
	}

}
