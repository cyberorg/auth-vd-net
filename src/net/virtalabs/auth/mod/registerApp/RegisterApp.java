package net.virtalabs.auth.mod.registerApp;

import net.virtalabs.auth.exceptions.AppException;
import net.virtalabs.auth.helpers.DbHelper;
import net.virtalabs.auth.helpers.TokenHelper;
import net.virtalabs.auth.models.Apps;
import net.virtalabs.auth.utils.C;

public class RegisterApp {

	public static String run(RegisterAppStruct modData) throws AppException {
		// check token
		// skipped now
		// get owner
		int ownerUid = TokenHelper.token2Uid(modData.getToken());
		if (ownerUid == 0) {
			throw new AppException("Wrong token supplied", C.WRONG_TOKEN);
		}
		// get mod Data as local vars to avoid NPE
		// init block

		// must be not null or not empty
		String redirectUrls = (modData.getApp().getRedirects() != null && modData
				.getApp().getRedirects().length() != 0) ? modData.getApp()
				.getRedirects() : "";

		if (redirectUrls.length() == 0) {
			throw new AppException("Redirect URLs cannot be empty",
					C.WRONG_JSON);
		}
		// making list by spliting them by space - left as example
		// List<String> validRedirects =
		// Arrays.asList(redirectUrls.split("\\s+"));

		// optional
		String name = (modData.getApp().getName() != null) ? modData.getApp()
				.getName() : "";
		String type = (modData.getApp().getType() != null) ? modData.getApp()
				.getType() : "standalone";
		String logo = (modData.getApp().getLogo() != null) ? modData.getApp()
				.getLogo() : "";
		String homeUrl = (modData.getApp().getUrl() != null) ? modData.getApp()
				.getUrl() : "";

		// DB
		DbHelper.open();

		// find record by homeUrl
		Apps oldApp = Apps.findFirst("appHomeUrl = ?", homeUrl);
		if (oldApp != null) {
			throw new AppException(
					"Application with same URL already exists. Please contact webteam@virtalabs.net",
					C.DB_DUPLICATE_ENTRY);
		}
		// ready to register new App
		// generate appId and appSecret
		long appId = Generator.generateAppId();
		String appSecret = Generator.generateAppSecret();

		Apps newApp = new Apps();
		newApp.set("appId", appId);
		newApp.set("appOwner", ownerUid);
		newApp.set("appSecret", appSecret);
		newApp.set("appName", name);
		newApp.set("appType", type);
		newApp.set("appLogo", logo);
		newApp.set("appHomeUrl", homeUrl);
		newApp.set("appRedirectUrls", redirectUrls);
		if (!newApp.save()) {
			throw new AppException("Unable to add new App", C.DB_SAVE);
		}
		// DB Close
		DbHelper.close();

		return Tx.talk(appId, appSecret, redirectUrls);
	}
}
