package net.virtalabs.auth.mod.registerApp;

import org.apache.commons.lang3.RandomStringUtils;

public class Generator {

	public static long generateAppId() {
		String IdStr = RandomStringUtils.randomNumeric(12);
		Long appId = Long.parseLong(IdStr);

		return appId;
	}

	public static String generateAppSecret() {
		int len = 15;

		String rnd = RandomStringUtils.randomAlphanumeric(len);
		return rnd;

	}

}
