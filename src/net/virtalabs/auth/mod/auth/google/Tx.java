package net.virtalabs.auth.mod.auth.google;

import com.google.gson.Gson;

public class Tx {
	private static final GoogleAuthTxStruct sTx = new GoogleAuthTxStruct();
	private static final Gson gson = new Gson();
	
	static String talk(String token){
		sTx.setCode(0);
		sTx.setMessage("Sending token");
		sTx.setToken(token);
		
		return gson.toJson(sTx);
	}
}
