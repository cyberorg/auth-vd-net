package net.virtalabs.auth.mod.auth.google;

import net.virtalabs.auth.exceptions.AppException;
import net.virtalabs.auth.helpers.DbHelper;
import net.virtalabs.auth.models.Tokens;
import net.virtalabs.auth.models.Users;
import net.virtalabs.auth.utils.C;

import com.google.gson.Gson;

public class GoogleAuth {
	private static Gson gson = new Gson();

	public static String run(GoogleAuthStruct appData) throws AppException {
		// 0)get code
		String code = appData.getCode();
		// 1)make request to Google Server (POST)
		String postBody = NetWork.makeLink(code);
		String tokenJson = NetWork.getAccessToken(postBody);
		if (tokenJson == null) {
			throw new AppException("Google is silent", C.GENERAL_ERROR);
		}
		// 2)handle Reply (access_token, expires_in)

		GoogleTokenStruct tokenStruct = gson.fromJson(tokenJson,
				GoogleTokenStruct.class);

		// 3) request to Google API and get User Info (email, name, surname)
		String authInfo = NetWork.getUserInfo(tokenStruct.getAccessToken(),
				tokenStruct.getTokenType());

		GoogleUserInfoStruct userInfoStruct = gson.fromJson(authInfo,
				GoogleUserInfoStruct.class);
		int expiryTS = (int) (System.currentTimeMillis() / 1000L)
				+ tokenStruct.getExpiresIn();

		/*
		 * String message = "E-mail: " + userInfoStruct.getEmail() +
		 * " First Name: " + userInfoStruct.getGivenName() + " Last Name: " +
		 * userInfoStruct.getFamilyName() + " Token: " +
		 * tokenStruct.getAccessToken() + " Expiry " + expiryTS;
		 */
		// 5)find out that user exists ? goto 7 : goto 6
		DbHelper.open();
		Users record = Users.findFirst("google = ?", userInfoStruct.getEmail());
		if (record == null) {
			// 6)create new record if not (store current user info, token,
			// expiry to
			Users user = new Users();
			user.set("login", userInfoStruct.getEmail());
			user.set("fname", userInfoStruct.getGivenName());
			user.set("lname", userInfoStruct.getFamilyName());
			user.set("google", userInfoStruct.getEmail());
			if (!user.saveIt()) {
				throw new AppException("Cannot add new Record", C.DB_SAVE);
			}
			// set user as record for step 7
			record = user;
		}
		// DB)
		// 7)update token and expiry
		int uid = record.getInteger("uid");
		Tokens token = Tokens.findFirst("uid = ?", uid);
		if (token == null) {
			// add new token
			Tokens newToken = new Tokens();
			newToken.set("provider", "google");
			newToken.set("token", tokenStruct.getAccessToken());
			newToken.set("uid", uid);
			newToken.set("expires", expiryTS);
			if (!newToken.saveIt()) {
				throw new AppException("Unable to add new token", C.DB_SAVE);
			}
		} else {
			// update current
			token.set("token", tokenStruct.getAccessToken());
			token.set("expires", expiryTS);
			if (!token.saveIt()) {
				throw new AppException("Unable to update token", C.DB_SAVE);
			}
		}
		// 8)return token
		return Tx.talk(tokenStruct.getAccessToken());
		// return stub
		// return StdTx.reply(0, message);
		// return "";

	}
}
