package net.virtalabs.auth.utils;

/**
 * Class with Codes (aka C++ header file, but in Java Way)
 * 
 * @author asm
 * 
 */
public class C {
	// General
	public final static int GENERAL_ERROR = 1;
	// Router
	public final static int NO_SUCH_ACTION = 2;
	public final static int NO_SUCH_PROVIDER = 3;
	// JSON
	public final static int EMPTY_JSON = 5;
	public final static int WRONG_JSON = 7;
	// Access Issues
	public final static int WRONG_TOKEN = 25;
	public final static int ACCESS_DENIED = 30;
	// DB Errors
	public final static int DB_CONNECT = 40;
	public final static int GET_DB_INFO = 41;
	public final static int DB_SAVE = 42;
	public final static int DB_DUPLICATE_ENTRY = 43;
	public final static int DB_ENTRY_NOT_FOUND = 44;
	public final static int DB_DELETE = 45;

	// Server-side
	public final static int CONFIG_ERROR = 51;
	public final static int NET_ERROR = 52;

}
