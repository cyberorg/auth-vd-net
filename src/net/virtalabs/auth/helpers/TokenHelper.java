package net.virtalabs.auth.helpers;

import net.virtalabs.auth.exceptions.AppException;
import net.virtalabs.auth.models.Apps;
import net.virtalabs.auth.utils.C;

public class TokenHelper {
	public static int token2Uid(String token) throws AppException {

		// TODO select where token = Token and provider = 'self';
		// Stub

		return 1;

		// return 0 if not found
		// return 0;
	}

	public static Apps uid2App(String token, long appId) throws AppException {

		int ownerUid = TokenHelper.token2Uid(token);
		if (ownerUid == 0) {
			throw new AppException("Wrong token supplied", C.WRONG_TOKEN);
		}

		Apps thisApp = Apps.findFirst("appId = ? AND appOwner = ?", appId,
				ownerUid);
		if (thisApp == null) {
			throw new AppException("No application with Id: " + appId
					+ " owned by UserId: " + ownerUid + " found",
					C.ACCESS_DENIED);
		}
		return thisApp;
	}
}
