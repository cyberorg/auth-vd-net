package net.virtalabs.auth.helpers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.HashMap;

import net.virtalabs.auth.exceptions.AppException;
import net.virtalabs.auth.utils.C;

import org.javalite.activejdbc.Base;
import org.javalite.activejdbc.InitException;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.reflect.TypeToken;

public class DbHelper {
	public static void open() throws AppException {
		HashMap<String, String> info = DbHelper.getInfo();
		try {
			if (!Base.hasConnection()) {
				Base.open(
						"com.mysql.jdbc.Driver",
						"jdbc:mysql://" + info.get("host") + "/"
								+ info.get("db"), info.get("user"),
						info.get("pass"));
			}
		} catch (InitException ie) {
			ie.printStackTrace(System.err);
			System.err.println("Connection to: " + info.get("host")
					+ " using username: " + info.get("user") + " failed");
			throw new AppException("Cannot connect to DB", C.DB_CONNECT);
		}
	}

	public static void close() {
		if (Base.hasConnection()) {
			Base.close();
		}
	}

	private static HashMap<String, String> getInfo() throws AppException {
		Gson gson = new Gson();
		Type type = new TypeToken<HashMap<String, String>>() {
		}.getType();
		HashMap<String, String> info = new HashMap<String, String>();
		try {
			FileReader fileReader = new FileReader(new File("cnf/db.json"));
			info = gson.fromJson(fileReader, type);
		} catch (FileNotFoundException fnfe) {
			throw new AppException(
					"File with db connect info not found. Make sure you copy it.",
					C.GET_DB_INFO);
		} catch (JsonIOException jioe) {
			throw new AppException("Json IO Exception occurs", C.GET_DB_INFO);
		}

		return info;
	}
}
